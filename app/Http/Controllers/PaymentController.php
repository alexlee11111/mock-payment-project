<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Redis;
use DB;

class PaymentController extends BaseController
{
    function index(){

    }

    function handlePayment(){

        $request = request()->all();

        //Retrieve data from form
        $currency = $request['currency'];

        $cc_no = $request['credit_card_no'];

        /*
            Case for select gatewayA:
                - curreny is 'HKD'/ 'AUD'/ 'EUR'/ 'JPY'/ 'CNY'
                - credit card is AMEX, card no. start with 37
        */
        if ($currency == 'USD' || $currency == 'AUD' ||
            $currency == 'EUR' || $currency == 'JPY' ||
            $currency == 'CNY' || substr($cc_no,2) == 37)
        {
            //Call gateway A
            $result = $this->callGatewayA($request);
            //$typeOf = gettype($result);
            //dd($typeOf);
        }
        else{
            //Call gateway B
            $result = $this->callGatewayB($request);
        }

        if ($result['result'] == 'success'){
            //Generate reference code
            $last_value = DB::table('transaction')
            ->orderBy('no','desc')
            ->select('no')
            ->first();
            //dd($last_value);
            if($last_value!=null){
                $last_value = $last_value->no;
            }
            else{
                $last_value = 0;
            }
            $ref_code = 'REF'.substr($request['customer_phone_no'],-3).substr($request['customer_phone_no'],-3).'061'.$last_value.'0719';

            //Create redis caching
            $redis  = Redis::connection();

            $customer_name = $request['customer_name'];
            $phone_no = $request['customer_phone_no'];
            $price = $request['price'];

            //Customer Name, Customer Phone Number, Currency and Price
            $redis->set($customer_name.':'.$ref_code, json_encode(
                array(
                    'customer_name' => $customer_name,
                    'customer_phone_no' => $phone_no,
                    'currency' => $currency,
                    'price' => $price
                )
            ), 'EX', 604800);

            //Retrieve success from gateway
            DB::table('transaction')->insert(
                [
                    'payment_ref_code' => $ref_code,
                    'customer_name' => $result['customer_name'],
                    'customer_phone_no' => $result['customer_phone_no'],
                    'currency' => $result['currency'],
                    'price' => $result['price']
                ]
            );

            //return $result;
            return response()->json(['result'=>$result, 'ref_code'=>$ref_code]);
        }
        else{
            return response()->json(['result'=>$result]);
        }
    }

    function callGatewayA($request)
    {
        $request_to_api = Request::create('/api/gatewayA', 'POST', $request);
        //$request_to_api = api('api.gatewayA', 'POST', $request->toArray());
        //$response = Route::dispatch($request_to_api);
        $response = app()->handle($request_to_api);
        $response = json_decode($response->getContent(),true);

        return $response;

    }

    function callGatewayB($request)
    {
        $request_to_api = Request::create('/api/gatewayB', 'POST', $request);
        //$request_to_api = api('api.gatewayB', 'POST', $request->toArray());
        //$response = Route::dispatch($request_to_api);
        $response = app()->handle($request_to_api);
        $response = json_decode($response->getContent(), true);
        

        return $response;
    }

    function test() {

        $currency = 'HKD';
        $price = 100;
        $cc_holder_name = 'LEE';
        $cc_no = 32323232;
        $cc_expiration = 'May 2018';
        $cc_CCV = 123;

        $request = [
            'payment_parameter' => [
                'currency' => $currency,
                'price' => $price,
                'credit_card_holdeing_name' => $cc_holder_name,
                'credit_card_no' => $cc_no,
                'credit_card_expiration' => $cc_expiration,
                'credit_card_CCV' => $cc_CCV
            ]
        ];

        return $request;
    }
}

