<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function ajaxRequest()

    {

        return view('test');

    }

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function ajaxRequestPost()

    {

        $input = request()->all();

        $result = $input['password'];

        $request_to_api = Request::create('/api/gatewayB', 'POST', $input);
        $response = app()->handle($request_to_api);
        $response = json_decode($response->getContent(), true);

        return response()->json(['result'=>$result, 'response' => $response]);

    }

}
