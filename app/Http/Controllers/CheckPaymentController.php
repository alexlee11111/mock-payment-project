<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Redis;
use DB;


class CheckPaymentController extends BaseController
{

    function index(){
        $data_json = null;
        return view('paymentCheck',compact('data_json'));

    }

    function checkRecord(){

        $request = request()->all();

        //get from value
        $name = $request['customer_name'];
        $code = $request['payment_reference_code'];

        //connect redis
        $redis  = Redis::connection();

        //get data string
        $data_string = $redis->get($name.':'.$code);

        if($data_string == null){
            $result = 'fail';
        }
        else{
            $result = 'success';
        }

        //pass string to json
        $data_json = json_decode($data_string,true);
        
        //return $data_json;

        return response()->json(['data'=>$data_json, 'result'=>$result]);

    }

    
}