<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

        <style>
            .error {
                color: #a94442;
            }
        </style>

    </head>
    <body>
        <div class="container">
            <form id='payment_form'>
                <!--Order Section-->
                <h1>Order Section</h1>
                <div class="form-group">
                    <label>Customer Name: </label>
                    <input name='customer_name' type='text'/>
                </div>
                <div class="form-group">
                    <label>Customer Phone Number: </label>
                    <input name='customer_phone_no' type='text'/>
                </div>
                <div class="form-group">
                    <label>Currency</label>
                    <select name='currency'>
                        <option value='HKD'>HKD</option>
                        <option value='USD'>USD</option>
                        <option value='AUD'>AUD</option>
                        <option value='EUR'>EUR</option>
                        <option value='JPY'>JPY</option>
                        <option value='CNY'>CNY</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <input name='price' type='text'/>
                </div>

                <!--Payment Section-->
                <h1>Payment Section</h1>
                <div class="form-group">
                    <label>Credit Card Holder Name: </label>
                    <input name='credit_card_holder_name' type='text'/>
                </div>
                <div class="form-group">
                    <label>Credit Card Number</label>
                    <input name='credit_card_no' type='text'/>
                </div>
                <div class="form-group">
                    <label>Credit Card Expiration</label>
                    <input name='credit_card_expiration_month' type='text' placeholder='MM'/> / 
                    <input name='credit_card_expiration_year' type='text' placeholder='YYYY'/>
                </div>
                <div class="form-group">
                    <label>Credit Card CCV</label>
                    <input name='credit_card_CCV' type='text'/>
                </div>

                <br/>
                <!--Submit button-->

                <!--<input type='submit' value='Submit'/>-->
                <button class="btn btn-success btn-submit btn-lg" data-toggle="modal" data-target="#myModal">Submit</button>

            </form>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id='response_result'/>
                        </div>
                        <div class="modal-body">
                            <p id='response_content'>
                            <b><p id='response_ref_code'/></b>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

    </body>

    <script>
        $(document).ready(function(){

        $("#payment_form").validate({
        rules: {
            customer_name: "required",
            customer_phone_no: {
                required: true,
                number: true
            },
            price: {
                required: true,
                number: true
            },
            credit_card_holder_name: 'required',
            credit_card_no: {
                required: true,
                maxlength: 16,
                minlength: 15,
                number: true
            },
            credit_card_expiration_month: {
                required: true,
                range: [1,12],
                number: true
            },
            credit_card_expiration_year: {
                required: true,
                number: true
            },
            credit_card_CCV: {
                required: true,
                number: true,
                maxlength: 4,
                minlength: 3
            }
        },
        messages: {
            //name: "Please input a valid name.",
        }
        })

        });
    </script>

    <script type='text/javascript'>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".btn-submit").click(function(e){
            e.preventDefault();

            var customer_name = $("input[name=customer_name]").val();
            var customer_phone_no = $("input[name=customer_phone_no]").val();
            var currency = $("select[name=currency]").val();
            var price = $("input[name=price]").val();
            var credit_card_holder_name = $("input[name=credit_card_holder_name]").val();
            var credit_card_no = $("input[name=credit_card_no]").val();
            var credit_card_expiration_month = $("input[name=credit_card_expiration_month]").val();
            var credit_card_expiration_year = $('input[name=credit_card_expiration_year]').val();
            var credit_card_CCV = $("input[name=credit_card_CCV]").val();

            //do the validation
            //$("#payment_form").valid();

            var current_year = (new Date()).getFullYear();
            var current_month = (new Date()).getMonth();

            if(current_year<= credit_card_expiration_year){
                if(current_year == credit_card_expiration_year){
                    if(current_month<=credit_card_expiration_month){

                    }
                    else{
                        $('#response_result').html('Error');
                        $('#response_content').html('Your card is expired. Please contact your card provider.');
                        return;
                    }
                }
            }
            else{
                $('#response_result').html('Error');
                $('#response_content').html('Your card is expired. Please contact your card provider.');
                return;
            }

            if(!$("#payment_form").valid()){
                //break the function if input not valid
                $('#response_result').html('Error');
                $('#response_content').html('Please check with the input fields.');
                return;
            }

            $.ajax({
                type:'POST',
                url:'/payment/post',
                data:{
                    customer_name:customer_name,
                    customer_phone_no:customer_phone_no,
                    currency:currency,
                    price:price,
                    credit_card_holder_name:credit_card_holder_name,
                    credit_card_no:credit_card_no,
                    credit_card_expiration_month:credit_card_expiration_month,
                    credit_card_expiration_year: credit_card_expiration_year,
                    credit_card_CCV:credit_card_CCV
                },

                success:function(data){
                    console.log(data);

                    if(data['result']['result'] == 'success'){
                        $result_text = 'SUCCESS';
                        $result_content = 'Please keep the folowing reference code for your checking.';
                    }
                    else{
                        $result_text = 'UNSUCCESS';
                        $result_content = 'Transaction process is not completed. Please check with your credit card information or contact your credit card provider.';
                    }

                    $('#response_ref_code').html(data['ref_code']);

                    $('#response_result').html($result_text);

                    $('#response_content').html($result_content);


                }
            });

        });
    </script>
</html>
