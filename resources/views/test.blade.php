<!DOCTYPE html>

<html>

<head>

    <title>Laravel 5.5 Ajax Request example</title>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <style>
        .error {
            color: #a94442;
        }
        .has-error .help-block { 
            color: #737373;
        }
    </style>
</head>

<body>

    <div class="container">

        <h1>Laravel 5.5 Ajax Request example</h1>



        <form id='payment_form'>

            <div class="form-group">

                <label>Name:</label>

                <input type="text" name="name" class="form-control" placeholder="Name">

            </div>

            <div class="form-group">
                <label>Currency</label>
                <select name='currency'>
                    <option value='HKD'>HKD</option>
                    <option value='USD'>USD</option>
                    <option value='JPY'>JPY</option>
                </select>
            <div>

            <div class="form-group">

                <label>Password:</label>

                <input type="password" name="password" class="form-control" placeholder="Password" required="">

            </div>

            <div class="form-group">

                <strong>Email:</strong>

                <input type="email" name="email" class="form-control" placeholder="Email" required="">

            </div>

            <div class="form-group">

                <button class="btn btn-success btn-submit" data-toggle="modal" data-target="#myModal">Submit</button>

            </div>

        </form>

    </div>

    <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
        <p id='response_name'/>
        <p id='response_password'/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>

</body>

<script>
    $(document).ready(function(){

    $("#payment_form").validate({
    rules: {
        name: "required"
    },
    messages: {
        name: "Please specify your name",
    }
    })

    $('#btn').click(function() {
        $("#payment_form").valid();
    });

    });
</script>

<script type="text/javascript">



    $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });



    $(".btn-submit").click(function(e){

        e.preventDefault();



        var name = $("input[name=name]").val();

        var password = $("input[name=password]").val();

        var email = $("input[name=email]").val();

        var currency = $("select[name=currency]").val();

        console.log('Email');
        console.log(email);

        //do the validation
        $("#payment_form").valid();

        if(name == '' || password == '' || email == ''){
            
            return;
        }

        $.ajax({

           type:'POST',

           url:'/ajaxRequest',

           data:{name:name, password:password, email:email, currency:currency},

           success:function(data){

              //alert(data['response']['name']);

              console.log(data);

                $('#response_name').html(data['response']['name']);
                $('#response_password').html(data['response']['password']);

           }

        });



	});

</script>


</html>
