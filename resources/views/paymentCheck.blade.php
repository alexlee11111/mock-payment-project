<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <h1>Payment Checking Form</h1>

            <form id='check_form'>
                <label>Customer Name: </label>
                <input type='text' name='customer_name'/>

                <br/>

                <label>Payment Reference Code: </label>
                <input type='text' name='payment_reference_code'/>

                <br/>
                <!--Submit button-->

                <!--<input type='submit' value='Submit'/>-->
                <button class="btn btn-success btn-submit btn-lg" data-toggle="modal" data-target="#myModal">Submit</button>

            </form>

            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" id='response_result'/>
                        </div>
                        <div class="modal-body">
                            <p id='response_content'>
                            <p>Customer Name: <span id='result_customer_name'/></p>
                            <p>Customer Phone No.: <span id='result_customer_phone_no'/></p>
                            <p>Currency: <span id='result_currency'/></p>
                            <p>Price: <span id='result_price'/></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    
                </div>
            </div>

        </div>
    </body>

    
    <script type='text/javascript'>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".btn-submit").click(function(e){
            e.preventDefault();

            var customer_name = $("input[name=customer_name]").val();
            var payment_reference_code = $("input[name=payment_reference_code]").val();

            $.ajax({
                type:'POST',
                url:'/paymentCheck/check',
                data:{
                    customer_name:customer_name,
                    payment_reference_code: payment_reference_code
                },

                success:function(data){
                    console.log(data);

                    if(data['result'] == 'success'){
                        $result_text = 'SUCCESS';
                        $result_content = 'Query result: ';
                        $result_customer_name = data['data']['customer_name'];
                        $result_customer_phone_no = data['data']['customer_phone_no'];
                        $result_currency = data['data']['currency'];
                        $result_price = data['data']['price'];
                    }
                    else{
                        $result_text = 'UNSUCCESS';
                        $result_content = 'No Record Found';
                        $result_customer_name = '-';
                        $result_customer_phone_no = '-';
                        $result_currency = '-';
                        $result_price = '-';
                    }

                    $('#response_result').html($result_text);
                    $('#response_content').html($result_content);
                    $('#result_customer_name').html($result_customer_name);
                    $('#result_customer_phone_no').html($result_customer_phone_no);
                    $('#result_currency').html($result_currency);
                    $('#result_price').html($result_price);

                }
            });

        });
    </script>
</html>
