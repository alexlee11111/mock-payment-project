<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/payment', function () {
    return view('payment');
});

Route::post('/payment/post', 'PaymentController@handlePayment');

/*
Route::get('/payment/post', function (Request $request) {
    return '12321';
});
*/

Route::get('/paymentCheck', 'CheckPaymentController@index');

Route::post('/paymentCheck/check', 'CheckPaymentController@checkRecord');

Route::get('/test','PaymentController@test');

Route::get('/test2', function() {
    $request = Request::create('/api/test', 'GET');

    $response = Route::dispatch($request);
    return $response;
});

Route::post('/testView/123321', function(Request $request) {
    //return $request;
    $request_to_api = Request::create('/api/test2', 'POST', $request->toArray()); 
    $response = dispatch($request_to_api);
    return $response;
});

route::get('/testView', function() {
    return view('test');
});

route::post('/testView/test', function(Request $request){
    return $request;
});

Route::get('/ajaxRequest', 'HomeController@ajaxRequest');

Route::post('/ajaxRequest', 'HomeController@ajaxRequestPost');