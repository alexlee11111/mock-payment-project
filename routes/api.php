<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/gatewayA', function (Request $request){
    //dd($request->all());
    $cc_no = $request->credit_card_no;
    $checker = substr($cc_no, -2);

    if($checker == 10){
        $result = 'success';
    }
    else{
        $result = 'fail';
    }
    $request->merge(['result' => $result]);

    return $request;
});

Route::post('/gatewayB', function (Request $request){
    //dd($request->all());
    $cc_no = $request->credit_card_no;
    $checker = substr($cc_no, -2);

    if($checker == 10){
        $result = 'success';
    }
    else{
        $result = 'fail';
    }
    $request->merge(['result' => $result]);

    return $request;
});

Route::get('/test','PaymentController@test');

Route::post('/test2', function (Request $request){
    //dd($request->all());
    if($request->name == 'Alex'){
        $result = 'success';
    }
    else{
        $result = 'fail';
    }
    $request->merge(['result' => $result]);
    return $request;
});